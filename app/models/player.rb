class Player < ApplicationRecord
  belongs_to :game
  has_one :board
end
